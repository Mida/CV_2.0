<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Formulaire Mot</title>
  <link href="style.css" rel="stylesheet">
</head>

<body>

  <form action="update_mot.php" method="GET">
    <label for="mot">Modification du mot :</label>
    <input name="mot" type="text">
    <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
    <input type="submit" class="sub">
  </form>

  <div>Retour à <a href="mot.php"> la page "Mot"</div>

</body>

</html>
