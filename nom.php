<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Page penduApp Nom</title>
  <link href="style.css" rel="stylesheet">
</head>

<body>

<ul>
  <?php
    ini_set("display_errors",true);
    $handle=mysqli_connect("localhost","root","mathildemysql","penduApp");
    $query="SELECT * FROM joueurs";
    $result=mysqli_query($handle,$query);
    while($line=mysqli_fetch_array($result)) {
      echo "\t <li>";
      echo "[" . $line["id"] . "] ";
      echo $line["nom"] . " ";
      echo "<a href=\"delete_nom.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "|";
      echo "<a href=\"form_nom.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li> \n";
    }
  ?>
</ul>

<form action="create_nom.php" method="GET">
  <label for="nom">Nouveau Nom :</label>
  <input name="nom" type="text">
  <input type="submit" class="sub">
</form>

<div>Retour à la page <a href="index.php"> d'accueil</div>

<!--
ASTUCE
Value = Placeholder
<input type="text" name="mot" value="<du PHP>">
<input type="hidden" name="id" value="<du PHP, id du mot>">
!-->

</body>

</html>
