<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Page d'administration penduApp</title>
  <link href="style.css" rel="stylesheet">
</head>

<body>

  <h1>Application "Pendu"</h1>

  <p>
    Ac ne quis a nobis hoc ita dici forte miretur, quod alia quaedam in hoc
     facultas sit ingeni, neque haec dicendi ratio aut disciplina, ne nos quidem
     huic uni studio penitus umquam dediti fuimus. Etenim omnes artes, quae ad
     humanitatem pertinent, habent quoddam commune vinculum, et quasi cognatione
     quadam inter se continentur.
  </p>

  <div>Accès à la page <a href="nom.php" title="Nom"> Nom</a></div>
  <div>Accès à la page <a href="mot.php" title="Mot"> Mot</a></div>

</body>

</html>
