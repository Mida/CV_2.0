<!doctype html>
<html>

<head>
  <meta charset="utf-8">
  <title>Page penduApp Mot</title>
  <link href="style.css" rel="stylesheet">
</head>

<body>

<ul>
  <?php
    ini_set("display_errors",true);
    $handle=mysqli_connect("localhost","root","mathildemysql","penduApp");
    $query="SELECT * FROM mots";
    $result=mysqli_query($handle,$query);
    while($line=mysqli_fetch_array($result)) {
      echo "\t <li>";
      echo "[" . $line["id"] . "] ";
      echo $line["mot"] . " ";
      echo "<a href=\"delete_mot.php?id=" . $line["id"] . "\"> Supprimer </a>";
      echo "<a href=\"form_mot.php?id=" . $line["id"] . "\"> Modifier </a>";
      echo "</li> \n";
    }
  ?>
</ul>

<form action="create_mot.php" method="GET">
  <label for="mot">Nouveau Mot :</label>
  <input name="mot" type="text">
  <input type="submit" class="sub">
</form>

<div>Retour à la page <a href="index.php"> d'accueil</div>

<!--
ASTUCE
Value = Placeholder
<input type="text" name="nom" value="<du PHP>">
<input type="hidden" name="id" value="<du PHP, id du nom>">
!-->

</body>

</html>
